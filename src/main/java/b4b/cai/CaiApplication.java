package b4b.cai;

import b4b.cai.scheduler.CustomTask;
import b4b.cai.scheduler.DemoExecutor;
import b4b.cai.scheduler.QuartzScheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
public class CaiApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CaiApplication.class, args);

    }



}
