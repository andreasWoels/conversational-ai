package b4b.cai.rest;

import b4b.cai.controller.MessageBuilder;
import b4b.cai.controller.MessageHandler;
import b4b.cai.model.*;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Ref;
import java.text.ParseException;
import java.util.List;

@RestController
public class RestInterface {

    @GetMapping("/health")
    public ResponseEntity<String> health() {
            return new ResponseEntity<>("Server running as expected",HttpStatus.OK);
    }


    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody DefaultBody payload) throws IOException {

        String result = MessageHandler.getInstance().register(payload);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody DefaultBody payload) throws IOException {

        String result = MessageHandler.getInstance().login(payload);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/greeting")
    public ResponseEntity<String> greet(@RequestBody DefaultBody payload) throws IOException {

        String result = MessageHandler.getInstance().greet(payload);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping("/{id}/deactivationskillquestion")
    public ResponseEntity<String> deactivateSkillQuestion(@PathVariable("id") Integer id) throws IOException {

        MessageHandler.getInstance().deactivateSkillQuestion(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("/skills")
    public ResponseEntity<String> getSkills(@RequestBody DefaultBody payload) throws IOException {
        String result = MessageHandler.getInstance().getSkills(payload);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }


    @GetMapping("/users/without/{id}")
    public ResponseEntity<String> getUsers(@PathVariable("id") Integer id) {

        String result = MessageHandler.getInstance().getUsers(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<String> login(@PathVariable("id") Integer id) {

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list/assign")
    public ResponseEntity<String> checkUsers(@PathVariable("id") Integer id, @RequestBody Text text) throws IOException {

        String result = MessageHandler.getInstance().checkUsers(id, text);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list/assign/{idsec}")
    public ResponseEntity<String> makeAssignment(@PathVariable("id") Integer id, @PathVariable("idsec") Integer assignee, @RequestBody DefaultBody body) throws IOException, JSONException {

        String result = MessageHandler.getInstance().assign(id, assignee, body);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list/duration")
    public ResponseEntity<String> addDuration(@PathVariable("id") Integer id, @RequestBody Reflection reflection) throws IOException {

        String result = MessageHandler.getInstance().addDuration(id, reflection);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list")
    public ResponseEntity<String> createTask(@PathVariable("id") Integer id, @RequestBody Task task) throws IOException, JSONException {

        String result = MessageHandler.getInstance().createTask(id, task);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/user/{id}/list")
        public ResponseEntity<String> getTasklist(@PathVariable("id") Integer id) {

        String result = MessageHandler.getInstance().getTasklist(id, null);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list/memory")
    public ResponseEntity<String> getTasklistWithMemory(@PathVariable("id") Integer id, @RequestBody DefaultBody body) {

        String result = MessageHandler.getInstance().getTasklist(id, body.getConversation().getMemory());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/user/{id}/list/delete")
    public ResponseEntity<String> deleteTask(@PathVariable("id") Integer id, @RequestBody Text text) throws IOException {

        String result = MessageHandler.getInstance().deleteTask(id, text);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/question/{id}")
    public ResponseEntity<String> getQuestion(@PathVariable("id") Integer id, @RequestBody Text text) throws IOException, ParseException {

        String result = MessageHandler.getInstance().getQuestion(id, text);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping("/answer/{id}")
    public ResponseEntity<String> setAnswer(@PathVariable("id") Integer id, @RequestBody Reflection reflection) throws IOException, ParseException {

        String result = MessageHandler.getInstance().setAnswer(id, reflection);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
    @GetMapping("/reflections/{id}")
    public ResponseEntity<String> getReflection(@PathVariable("id") Integer id) throws IOException {

        String result = MessageHandler.getInstance().getReflection(id);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @GetMapping(value = "/low.png", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getLowPrioIcon() throws IOException {
        InputStream in = getClass().getResourceAsStream("/icons/low.png");
        return IOUtils.toByteArray(in);
    }

    @GetMapping(value = "/medium.png", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getMediumPrioIcon() throws IOException {
        InputStream in = getClass().getResourceAsStream("/icons/medium.png");
        return IOUtils.toByteArray(in);
    }

    @GetMapping(value = "/high.png", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getHighPrioIcon() throws IOException {
        InputStream in = getClass().getResourceAsStream("/icons/high.png");
        return IOUtils.toByteArray(in);
    }

}