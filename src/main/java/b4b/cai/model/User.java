package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class User {

    public String getConversationId() {
        return conversationId;
    }

    private String conversationId;
    private String name;
    private int id;
    private List<Task> list;
    List<LocalDateTime> attendance;
    List<Task> history;

    public HashMap<String, Boolean> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(HashMap<String, Boolean> configuration) {
        this.configuration = configuration;
    }

    HashMap<String, Boolean> configuration;

    public List<Reflection> getReflections() {
        return reflections;
    }

    public void setReflections(List<Reflection> reflections) {
        this.reflections = reflections;
    }

    private List<Reflection> reflections;

    public User(String name, String conversationId) {
        this.conversationId = conversationId;
        this.name = name.toLowerCase();
        this.list = new ArrayList<>();
        this.reflections = new ArrayList<>();
        this.attendance = new ArrayList<>();
        this.history = new ArrayList<>();
        this.configuration = new HashMap<>();

        this.configuration.put("skillquestion", true);
        this.configuration.put("sentimentquestion", false);
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<Task> getList() {
        return list;
    }

    public void setList(List<Task> list) {
        this.list = list;
    }

    public List<LocalDateTime> getAttendance() {
        return attendance;
    }
    public void setAttendance(List<LocalDateTime> list) {
        this.attendance = list;
    }

    public List<Task> getHistory() {
        return history;
    }

    public void setHistory(List<Task> history) {
        this.history = history;
    }

}
