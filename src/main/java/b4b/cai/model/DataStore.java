package b4b.cai.model;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataStore {

    private static DataStore instance;
    private static List<User> users;



    public DataStore(List<User> users, Gson gson) {

        this.users = users;

    }

    public static DataStore getInstance(){

        if(instance == null){
            instance = new DataStore(new ArrayList<User>(), new Gson());
        }
        return instance;
    }


    public List<User> getUsers() {

        return this.users;
    }

    public User addUser(User user){

        Integer freeId = DataStore.getInstance().getUsers().size() + 1;
        user.setId(freeId);
        this.users.add(user);
        return user;
    }

    public List<User> getUserByID(Integer id){

        try {
            List<User> list = this.users.stream()
                    .filter(item -> item.getId().equals(id))
                    .collect(Collectors.toList());
            return list;
        }catch (NullPointerException e){

            return new ArrayList<User>();
        }
    }
    public List<User> getUserByName(String name){

        try {
            List<User> list = this.users.stream()
                    .filter(item -> item.getName().equals(name))
                    .collect(Collectors.toList());
            return list;
        }catch (NullPointerException e){

            return new ArrayList<User>();
        }
    }
}
