package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Reflection {

    public Reflection(@JsonProperty("question") String question, @JsonProperty("answer") String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    private String question;
    private String answer;
}
