package b4b.cai.model;

import b4b.cai.model.conversational_ai.Conversation;
import b4b.cai.model.conversational_ai.Reply;

import java.util.List;

public class MessageFormat {

    private List<Reply> replies;

    public List<Reply> getReplies() {
        return replies;
    }

    public Conversation getConversation() {
        return conversation;
    }

    private Conversation conversation;

    public MessageFormat(List<Reply> replies, Conversation conversation) {
        this.replies = replies;
        this.conversation = conversation;
    }
}





/* Format for Response Message
* {
  "replies": [
    {
      "type": "text",
      "content": "Hello world!"
    }
  ],
  "conversation": {
    "language": "en",
    "memory": {
      "user": "Bob"
    }
  }
}*/