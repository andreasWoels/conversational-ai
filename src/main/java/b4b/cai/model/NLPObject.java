package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class NLPObject {

    public String getSource() {
        return source;
    }

    private String source;
    private String sentiment;
    private String act;
    private String type;
    private String version;
    private String processing_language;
    private String language;
    private String uuid;
    private Integer status;
    private String timestamp;
    private List<Intent> intentList;
    private Entity entity;

    public NLPObject(@JsonProperty("source")String source, @JsonProperty("sentiment") String sentiment,
                     @JsonProperty("act") String act, @JsonProperty("type")String type,
                     @JsonProperty("version")String version, @JsonProperty("processing_language")String processing_language,
                     @JsonProperty("language")String language, @JsonProperty("uuid") String uuid,
                     @JsonProperty("status")Integer status, @JsonProperty("timestamp") String timestamp,
                     @JsonProperty("intentList")List<Intent> intentList, @JsonProperty("entity") Entity entity) {
        this.source = source;
        this.sentiment = sentiment;
        this.act = act;
        this.type = type;
        this.version = version;
        this.processing_language = processing_language;
        this.language = language;
        this.uuid = uuid;
        this.status = status;
        this.timestamp = timestamp;
        this.intentList = intentList;
        this.entity = entity;
    }
}