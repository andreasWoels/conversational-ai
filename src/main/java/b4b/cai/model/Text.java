package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Text {

    public Text(@JsonProperty("text") String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    private String text;

}
