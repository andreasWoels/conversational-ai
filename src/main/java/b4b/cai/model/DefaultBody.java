package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class DefaultBody {

    public CaiConversation getConversation() {
        return conversation;
    }

    private CaiConversation conversation;

    public NLPObject getNlp() {
        return nlp;
    }

    private NLPObject nlp;


    public DefaultBody(@JsonProperty("conversation") CaiConversation conversation, @JsonProperty("nlp") NLPObject nlp) {

        this.nlp = nlp;
        this.conversation = conversation;
    }
}
