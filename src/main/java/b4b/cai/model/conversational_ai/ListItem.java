package b4b.cai.model.conversational_ai;

import java.util.List;

public class ListItem {

    private String title;
    private String imageUrl;
    private String subtitle;
    private List<Button> buttons;

    public ListItem(String title, String imageUrl, String subtitle, List<Button> buttons) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.subtitle = subtitle;
        this.buttons = buttons;
    }
}
