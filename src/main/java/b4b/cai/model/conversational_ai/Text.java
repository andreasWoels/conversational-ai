package b4b.cai.model.conversational_ai;

public class Text {

    private String type;
    private Integer delay;
    private String content;

    public String getType() {
        return type;
    }

    public Integer getDelay() {
        return delay;
    }

    public String getText() {
        return content;
    }

    public Text(String content) {
        this.content = content;
        this.type = "text";
    }

}
