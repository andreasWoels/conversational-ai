package b4b.cai.model.conversational_ai;

public class CaiUser {
    private Integer id;
    private String fullname;
    private String raw;
    private Double confidence;

    public CaiUser(Integer id, String name, Double confidence) {
        this.id = id;
        this.fullname = name;
        this.raw = name;
        this.confidence = confidence;
    }
}
