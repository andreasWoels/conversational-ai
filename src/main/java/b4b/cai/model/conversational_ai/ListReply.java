package b4b.cai.model.conversational_ai;
public class ListReply extends Reply {

    private Content content;

    public ListReply(Content content) {
        super();
        this.type = "list";
        this.content = content;


    }

}
