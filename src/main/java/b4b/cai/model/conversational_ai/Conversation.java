package b4b.cai.model.conversational_ai;

public class Conversation {

        private String language;
        private Memory memory;

        public String getLanguage() {
                return language;
        }

        public Memory getMemory() {
                return memory;
        }

        public Conversation() {
                this.language = "en";
                this.memory = new Memory();
        }

        public Conversation(Memory memory) {
                this.language = "en";
                this.memory = memory;
        }
}
