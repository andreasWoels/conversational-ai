package b4b.cai.model.conversational_ai;

import java.util.List;

public class Content {

    public Content(List<Button> buttons, List<ListItem> elements) {
        this.buttons = buttons;
        this.elements = elements;
    }

    private String content;
    private List<Button> buttons;
    private List<ListItem> elements;
}
