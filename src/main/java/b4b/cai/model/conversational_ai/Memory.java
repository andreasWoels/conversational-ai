package b4b.cai.model.conversational_ai;

import b4b.cai.model.Task;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Memory {

    private String question;
    private String sk;
    private String data;
    private Person taskperson;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    private String id;

    public Memory(@JsonProperty("id")String id, @JsonProperty("taskperson")Person person){
        this.id = id;
        this.taskperson = person;
    }

    public Memory(){}

    public void setQuestion(String question) {
        this.question = question;
    }
}
