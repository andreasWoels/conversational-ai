package b4b.cai.model.conversational_ai;

public class TextReply extends Reply{

    private String content;
    private Integer delay;

    public TextReply(String content) {
        super();
        this.type = "text";
        this.content = content;
        this.delay = 1;
    }

}
