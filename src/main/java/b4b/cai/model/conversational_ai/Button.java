package b4b.cai.model.conversational_ai;

public class Button {

    private String title;
    private String type;
    private String value;

    public Button(String title, String type, String value) {
        this.title = title;
        this.type = type;
        this.value = value;
    }
}
