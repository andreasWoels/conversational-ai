package b4b.cai.model.conversational_ai;

import java.util.ArrayList;
import java.util.List;

public class Elements {

    public Elements(List<ListItem> elements) {
        this.elements = elements;
    }

    private List<ListItem> elements;


    public Elements(ListItem element) {
        this.elements = new ArrayList<>();
        this.elements.add(element);
    }
}
