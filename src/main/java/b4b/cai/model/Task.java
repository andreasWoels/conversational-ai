package b4b.cai.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.lang.Integer.parseInt;

public class Task {

    private String name;
    private String priority;
    private String icon;
    private LocalDateTime date;
    private String duration;

    private Integer durationInSeconds;

    private String realduration;

    private LocalDateTime created;
    private LocalDateTime deleted;

    public Integer getDurationInSeconds() {
        return durationInSeconds;
    }

    public Task(@JsonProperty("name") String name, @JsonProperty("priority") String priority,
                @JsonProperty("date") Date date, @JsonProperty("duration") String duration,
                @JsonProperty("durationInSeconds") String durationInSeconds) {

        this.name = name;
        this.priority = priority;
        if(priority == null){
            priority = "low";
        }
        this.icon = setIcon(priority);
        if(date == null){
            this.date = LocalDateTime.now();
        }
        else{
            this.date =  LocalDateTime.ofInstant(date.toInstant(),
                    ZoneId.systemDefault());
        }

        this.duration = duration;
        this.created = LocalDateTime.now();

        this.durationInSeconds = Math.round(Float.parseFloat(durationInSeconds));
    }

    public String getName() {

        return name;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public String getIcon() {
        return icon;
    }

    public String getDuration() {
        return duration;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDeleted(LocalDateTime deleted) {
        this.deleted = deleted;
    }

    public String setIcon(String priority){

        String iconURL = "";
        switch (priority){
            case "high":
                iconURL = "https://caitest.azurewebsites.net/high.png";
                break;
            case "medium":
                iconURL = "https://caitest.azurewebsites.net/medium.png";
                break;
            case "low":
                iconURL = "https://caitest.azurewebsites.net/low.png";
                break;
        }
        return iconURL;
    }

    public String getRealduration() {
        return realduration;
    }

    public void setRealduration(String realduration) {
        this.realduration = realduration;
    }
}
