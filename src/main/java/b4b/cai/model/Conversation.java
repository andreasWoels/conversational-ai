package b4b.cai.model;

public class Conversation {
    public String getChannel() {
        return channel;
    }

    public String getChatId() {
        return chatId;
    }

    public String getConnector() {
        return connector;
    }

    public Boolean getFallbacked() {
        return fallbacked;
    }

    public String getId() {
        return id;
    }

    private String channel;
    private String chatId;
    private String connector;
    private Boolean fallbacked;
    private String id;
}
