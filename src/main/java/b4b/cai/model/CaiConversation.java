package b4b.cai.model;

import b4b.cai.model.conversational_ai.Memory;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CaiConversation {

    public String getId() {
        return id;
    }

    private String id;
    private String language;

    public Memory getMemory() {
        return memory;
    }

    private Memory memory;
    private List<String> skill_stack;
    private String skill;
    private Integer skill_occurences;

    public CaiConversation(@JsonProperty("id")String id, @JsonProperty("language")String language,@JsonProperty("memory") Memory memory,
                           @JsonProperty("skill_stack")List<String> skill_stack, @JsonProperty("skill")String skill,
                           @JsonProperty("skill_occurences")Integer skill_occurences) {
        this.id = id;
        this.language = language;
        this.memory = memory;
        this.skill_stack = skill_stack;
        this.skill = skill;
        this.skill_occurences = skill_occurences;
    }
}


