package b4b.cai.model;

import java.util.List;

public class ConversationResponse {

    private String message;
    private List<Conversation> results;

    public List<Conversation> getResults() {
        return results;
    }
}
