package b4b.cai.controller;
import b4b.cai.model.*;
import b4b.cai.model.Conversation;
import b4b.cai.model.Text;
import b4b.cai.model.conversational_ai.*;
import b4b.cai.rest.CaiInterface;
import org.json.JSONException;

import javax.swing.text.MutableAttributeSet;
import java.io.IOException;
import java.sql.Array;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import static java.lang.Integer.parseInt;

public class MessageHandler {
    
    private static final String ERR_MSG_SERVER_ERROR = "Server Error: Please write an email to andreas.woels@b4b-solutions.com";

    private static MessageHandler instance;
    public static MessageHandler getInstance(){

        if(instance == null){
            return new MessageHandler();
        }
        return instance;
    }

    public String register(DefaultBody payload) throws IOException {

        User user = new User(payload.getNlp().getSource(), payload.getConversation().getId());
        DataStore.getInstance().addUser(user);

        List<Reply> textreplies = new ArrayList<>();
        TextReply greeting = new TextReply("Nice to meet you " + payload.getNlp().getSource() + "!");
        textreplies.add(greeting);

        TextReply skills = new TextReply("Are you familiar with my skills?");
        textreplies.add(skills);

        MessageFormat response = MessageBuilder.getInstance().buildTextWithMemory(textreplies, "13", user.getId().toString());

        return HelperClass.getInstance().getGson().toJson(response);

    }
    public String login(DefaultBody payload) throws IOException {

        String conversationId = payload.getConversation().getId();

        List<User> filteredUsers = DataStore.getInstance().getUsers().stream().filter(x -> x.getConversationId().equals(conversationId)).collect(Collectors.toList());

        if(filteredUsers.isEmpty()){
            TextReply text = new TextReply("My name is Reflector.");
            TextReply text2 = new TextReply("What's your name?");
            List<Reply> replies = Arrays.asList(text, text2);
            MessageFormat response = MessageBuilder.getInstance().buildTextWithMemory(replies, "2", null);
            return HelperClass.getInstance().getGson().toJson(response);
        }
        else{

            String skill = "";
            List<Reply> replies = new ArrayList<>();
            replies.add(new TextReply("Nice to meet you " + filteredUsers.get(0).getName() + "!"));
            if(filteredUsers.get(0).getConfiguration().get("skillquestion")){
                TextReply skills = new TextReply("Are you familiar with my skills?");
                replies.add(skills);
                skill = "13";
            }
            else if(filteredUsers.get(0).getConfiguration().get("sentimentquestion") && (LocalDateTime.now().getDayOfMonth() % 2) == 0){

                TextReply skills = new TextReply("How do you do?");
                replies.add(skills);
                skill = "16";
            }
            else if(filteredUsers.get(0).getConfiguration().get("sentimentquestion") && (LocalDateTime.now().getDayOfMonth() % 2) != 0){

                TextReply skills = new TextReply("Do you want to see your To-Do List?");
                replies.add(skills);
                skill = "20";
            }
            MessageFormat response = MessageBuilder.getInstance().buildTextWithMemory(replies, skill , filteredUsers.get(0).getId().toString());
            return HelperClass.getInstance().getGson().toJson(response);
        }
    }

    public String greet(DefaultBody payload) throws IOException {

        List<Reply> replies = new ArrayList<>();
        TextReply skills = new TextReply("Hi");
        replies.add(skills);
        MessageFormat response = MessageBuilder.getInstance().buildTextWithoutUser(replies);
        return HelperClass.getInstance().getGson().toJson(response);
    }

    public String getUsers(Integer id){

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            String responseJson = HelperClass.getInstance().getGson().toJson(
                    MessageBuilder.getInstance().buildUserList(
                            registeredUser.get(0))
            );
            return responseJson;
        }
        return "Server Error: Please write an email to andreas.woels@b4b-solutions.com";
    }

    public String createTask(Integer userid, Task task) throws IOException, JSONException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(userid);
        if(!registeredUser.isEmpty()){

            if(task.getDate() == null){
                LocalDateTime now = LocalDateTime.now().plusYears(100);
                task.setDate(now);
            }
            else{

                task.setDate(task.getDate());
            }
            registeredUser.get(0).getList().add(task);

            String url = conversationalUrl + registeredUser.get(0).getConversationId() +"/messages";
            String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \" "+ "Your list has a new entry!"  +"\" }]}";
            String res = CaiInterface.getInstance().executeHTTPRequest(url, developerToken, message, "POST");

            List<Reply> replies = Arrays.asList(new TextReply("Your To-Do` List has a new entry!"));
            return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithMemory(replies,"", registeredUser.get(0).getId().toString()));
        }

        return ERR_MSG_SERVER_ERROR;
    }

    public String getTasklist(Integer id, Memory memory){

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            List<ListItem> caiList = MessageBuilder.getInstance().buildTaskList(registeredUser.get(0));

            String responseJson = HelperClass.getInstance().getGson().toJson(
                    MessageBuilder.getInstance().buildList(
                            registeredUser.get(0),
                            caiList, memory)
            );
            return responseJson;
        }
        return "Server Error: Please write an email to andreas.woels@b4b-solutions.com";
    }


    public String deleteTask(Integer id, Text task) throws IOException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);

        if(!registeredUser.isEmpty()){

            List<Task> usersList = registeredUser.get(0).getList();

            List<Task> filterdList = usersList.stream().filter(element -> element.getName().equals(task.getText()))
                    .collect(Collectors.toList());
            if(filterdList.isEmpty()){
                filterdList = usersList.stream().filter(element -> element.getName().contains(task.getText()))
                        .collect(Collectors.toList());
            }

            if (filterdList.isEmpty()){
                List<String> list = new ArrayList<>();
                list.add("I could not find the task \"" + task.getText() + "\"");
                return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildText(registeredUser.get(0), list, null));
            }
            else{

                filterdList.get(0).setDeleted(LocalDateTime.now());

                List<Task> history = registeredUser.get(0).getHistory();
                history.add(filterdList.get(0));
                registeredUser.get(0).setHistory(history);

                usersList.remove(filterdList.get(0));

                List<String> list = new ArrayList<>();
                String deletion = "I deleted \"" + filterdList.get(0).getName() + "\" from your list";
                list.add(deletion);

                String count = HelperClass.getInstance().determineTasksDone(LocalDate.now(), registeredUser.get(0));

                String motivation = "Well done. You have finished the " + count  + " task for today!";
                list.add(motivation);

                String duration = "How long did it take You to complete the task?";
                list.add(duration);

                return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildText(registeredUser.get(0), list, "11"));
            }
        }
        return ERR_MSG_SERVER_ERROR;
    }


    public String getQuestion(Integer id, Text text) throws IOException, ParseException {

        Integer question = parseInt(text.getText());
        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            if(question == 1){
                List<LocalDateTime> datetime = registeredUser.get(0).getAttendance();
                datetime.add(LocalDateTime.now());
                registeredUser.get(0).setAttendance(datetime);
            }

            return HelperClass.getInstance().getGson().toJson(
                    MessageBuilder.getInstance().buildQuestion(
                            registeredUser.get(0),
                            question)
            );
        }
        else{
            return ERR_MSG_SERVER_ERROR;
        }
    }

    public String setAnswer(Integer id, Reflection reflection) throws IOException, ParseException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            User activeUser = registeredUser.get(0);
            Integer counter = null;
            try {
                counter = parseInt(reflection.getQuestion());
            }
            catch (Exception e){
                counter = HelperClass.getInstance().getQuestionsForToday().size() - 1;
            }

            List<Reflection> reflections = activeUser.getReflections();
            if(counter == 0){
                Reflection seperator = new Reflection("Day " + LocalDate.now() + ": ", "----Seperation-----");
                reflections.add(seperator);
            }
            else{
                LocalTime now = LocalTime.now();
                boolean isBefore = now.isBefore(HelperClass.getInstance().getTimeseperator());

                List<String> questions = HelperClass.getInstance().getQuestionsForToday();
                String question = questions.get(counter);

                Reflection activeReflection = new Reflection(question, reflection.getAnswer());
                reflections.add(activeReflection);
            }
            counter += 1;

            return HelperClass.getInstance().getGson().toJson(
                    MessageBuilder.getInstance().buildAnswer(activeUser, counter));
        }

        return ERR_MSG_SERVER_ERROR;
    }

    public String getReflection(Integer id) throws IOException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            User activeUser = registeredUser.get(0);
            return HelperClass.getInstance().getGson().toJson(activeUser.getReflections());
        }

        return ERR_MSG_SERVER_ERROR;
    }

    public String getSkills(DefaultBody body) throws IOException {

        MessageFormat response =  MessageBuilder.getInstance().buildSkillList(body.getConversation().getMemory().getId());

        return HelperClass.getInstance().getGson().toJson(response);
    }

    public String deactivateSkillQuestion(Integer userid) throws IOException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(userid);

        HashMap<String, Boolean> config = registeredUser.get(0).getConfiguration();
        config.put("skillquestion", false);
        config.put("sentimentquestion", true);
        return "Success";

    }

    public String checkUsers(Integer id, Text user) throws IOException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            User activeUser = registeredUser.get(0);

            User assignee = HelperClass.getInstance().findUserByName(user.getText());

            if(assignee == null){
                Memory memory = new Memory();
                memory.setId(activeUser.getId().toString());
                List<Reply> list = new ArrayList<>();
                list.add(new TextReply("Sorry, but there is no user with this name."));
                return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithCustomMemory(list, memory));
            }
            else{
                Memory memory = new Memory();
                memory.setSk("18");
                memory.setId(activeUser.getId().toString());
                memory.setData(assignee.getId().toString());

                TextReply text = new TextReply("Which task do you want to assign to your colleague?");
                List<Reply> list = Arrays.asList(text);

                return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithCustomMemory(list, memory));
            }
        }

        return ERR_MSG_SERVER_ERROR;
    }

    public String assign(Integer id, Integer assignee, DefaultBody body) throws IOException, JSONException {

        List<User> registeredUser = DataStore.getInstance().getUserByID(id);
        if(!registeredUser.isEmpty()){

            User activeUser = registeredUser.get(0);

            List<User> assignees = DataStore.getInstance().getUserByID(assignee);
            if(!assignees.isEmpty()){

                List<Task> filter = activeUser.getList().stream().filter(x -> x.getName().toLowerCase().equals(body.getNlp().getSource().toLowerCase())).collect(Collectors.toList());
                if(!filter.isEmpty()){

                    List<Task> assigneeList = assignees.get(0).getList();
                    assigneeList.add(filter.get(0));
                    assignees.get(0).setList(assigneeList);

                    List<Task> list = activeUser.getList();
                    list.remove(filter.get(0));
                    activeUser.setList(list);
                    sendNotification(assignees.get(0), activeUser.getName());
                }
                else{
                    Memory memory = new Memory();
                    memory.setSk("17");
                    memory.setId(activeUser.getId().toString());
                    List<Reply> replies = Arrays.asList(new TextReply("Could not find the task. Please try again"));
                    return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithCustomMemory(replies, memory));
                }

                Memory memory = new Memory();
                memory.setId(activeUser.getId().toString());
                List<Reply> replies = Arrays.asList(new TextReply("I have assigned the task"));
                return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithCustomMemory(replies, memory));
            }
        }

        return ERR_MSG_SERVER_ERROR;
    }

    private String conversationalUrl = "https://api.cai.tools.sap/connect/v1/conversations/";
    private String developerToken = "1712fd146137df17c2e93aba0097bacb";


    public void sendNotification(User assigne, String senderName) throws IOException, JSONException {

        String url = conversationalUrl + assigne.getConversationId() +"/messages";

        String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \" "+ "You have got a new task in your To-Do List from " + senderName +  "\" }]}";
        String res = CaiInterface.getInstance().executeHTTPRequest(url, developerToken, message, "POST");

    }

    public String addDuration(Integer id, Reflection text) throws IOException {

        List<User> filteredUsers = DataStore.getInstance().getUsers().stream().filter(x -> x.getId().equals(id)).collect(Collectors.toList());

        if(!filteredUsers.isEmpty()){

            Task last = filteredUsers.get(0).getHistory().get(filteredUsers.get(0).getHistory().size() - 1);

            last.setRealduration(text.getQuestion());
            Memory memory = new Memory();
            memory.setId(id.toString());

            List<Reply> list = new ArrayList<>();

            Integer converted = Math.round(Float.parseFloat(text.getAnswer()));

            if(converted >  filteredUsers.get(0).getHistory().get(filteredUsers.get(0).getHistory().size() - 1).getDurationInSeconds() + 900){

                list.add(new TextReply("Ok. You needed longer as expected. Invest more time in the estimation of your task!"));
            }
            else if(converted + 900 <  filteredUsers.get(0).getHistory().get(filteredUsers.get(0).getHistory().size() - 1).getDurationInSeconds()){

                list.add(new TextReply("Ok. You have made your task very fast! Well Done!"));
            }
            else{
                list.add(new TextReply("Good Job. You have done your task in the estimated duration."));
            }


            return HelperClass.getInstance().getGson().toJson(MessageBuilder.getInstance().buildTextWithCustomMemory(list, memory));
        }
        else{
            return ERR_MSG_SERVER_ERROR;
        }
    }
}
