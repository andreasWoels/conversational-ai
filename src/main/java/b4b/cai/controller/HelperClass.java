package b4b.cai.controller;

import b4b.cai.model.Conversation;
import b4b.cai.model.DataStore;
import b4b.cai.model.User;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class HelperClass {


    private static HelperClass instance;
    private static Gson gson;

    private static LocalTime timeseperator;

    public static LocalTime getTimeseperator() {
        return timeseperator;
    }

    public HelperClass() {
        gson = new Gson();
        timeseperator = LocalTime.of(12,0,0);
    }


    public static Gson getGson() {
        return gson;
    }

    public static HelperClass getInstance() {

        if(instance == null){
            instance = new HelperClass();
        }
        return instance;
    }

    public LocalDateTime createLocalDate(String str){

        if(str.charAt(1) == '.'){
            str = '0' + str;
        }
        str = str.replace(".", "-") + "-00-00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm");

        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        return dateTime;
    }

    public List<String> getQuestionsForToday() throws IOException, ParseException {

        LocalTime now = LocalTime.now();
        boolean isBefore = now.isBefore(HelperClass.getInstance().getTimeseperator());

        List<List<String>> csv = null;
        if(isBefore){
            csv = CSVHandler.getInstance().getEarly();
        }
        else{
            csv = CSVHandler.getInstance().getLate();
        }

        for (List<String> element : csv) {

            LocalDateTime csv_date = HelperClass.getInstance().createLocalDate(element.get(0));

            LocalDate first = csv_date.toLocalDate();
            LocalDate second = LocalDate.now();
            if(HelperClass.getInstance().isSameDate(first, second)){
                return element;
            }
        }
        return null;
    }

    public String determineTasksDone(LocalDate today, User user){

        HashMap<Integer, String> motivation = new HashMap<Integer, String>();
        motivation.put(1, "first");
        motivation.put(2, "second");
        motivation.put(3, "third");

        Integer count = 0;
        for (Integer i = 0; i < user.getHistory().size(); i++){
            count += 1;
        }

        if(count >= 4){
            return "next";
        }
        else{
            return motivation.get(count);
        }
    }

    public Boolean isSameDate(LocalDate first, LocalDate second){

        if(first.compareTo(second) == 0){
            return true;
        }
        else{
            return false;
        }
    }



    public Boolean isSameDate(LocalDateTime first, LocalDateTime second){

        if(first.getYear() == second.getYear() && first.getMonth().equals(second.getMonth()) && first.getDayOfMonth() == second.getDayOfMonth()){
            return true;
        }

        return false;
    }

    public Boolean isOneDayBefore(LocalDateTime first, LocalDateTime second){

        first.plusDays(1);
        if(first.getYear() == second.getYear() && first.getMonth().getValue() == second.getMonth().getValue() && first.getDayOfMonth() == second.getDayOfMonth()){

                return true;
        }

        return false;
    }

    public User findCorrespondingUser(Conversation conversation){

        List<User> users = DataStore.getInstance().getUsers();

        List<User> filter = users.stream().filter(x -> x.getConversationId().equals(conversation.getId())).collect(Collectors.toList());

        if(filter.isEmpty()){
            return null;
        }
        else{
            return filter.get(0);
        }
    }

    public User findUserByName(String name){

        List<User> users = DataStore.getInstance().getUsers();

        List<User> filter = users.stream().filter(x -> x.getName().toLowerCase().equals(name.toLowerCase())).collect(Collectors.toList());

        if(filter.isEmpty()){
            return null;
        }
        else{
            return filter.get(0);
        }
    }

}
