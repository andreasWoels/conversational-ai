package b4b.cai.controller;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVHandler {

    private static CSVHandler instance;
    private static List<List<String>> early;
    private static List<List<String>> late;

    public static CSVHandler getInstance() throws IOException {

        if(instance == null){
            return new CSVHandler();
        }
        return instance;
    }

    public static List<List<String>> getLate() {
        return late;
    }

    public static List<List<String>> getEarly() {
        return early;
    }

    public CSVHandler() throws IOException {

        List<List<String>> document;

        File earlyFile = getFileFromResources("questions-early.csv");
        document =  read(earlyFile);
        early = convertedData(document);

        File lateFile = getFileFromResources("questions-late.csv");
        document =  read(lateFile);
        late = convertedData(document);
    }

    public File getFileFromResources(String filename) throws IOException {

        InputStream cpResource = this.getClass().getClassLoader().getResourceAsStream(filename);
        File tmpFile = File.createTempFile("file", "temp");
        FileUtils.copyInputStreamToFile(cpResource, tmpFile);
        return tmpFile;
    }


    public List<List<String>> convertedData(List<List<String>> document){
        List<List<String>> days = new ArrayList<>();

        for (Integer x = 0; x < document.size(); x++){

            for (Integer y = 0; y < document.get(x).size(); y++){

                if (x == 0){
                    days.add(new ArrayList<>());
                }
                days.get(y).add(document.get(x).get(y));
            }
        }

        return days;
    }

    public List<List<String>> read(File file) throws IOException {

        List<List<String>> records = new ArrayList<List<String>>();
        try (
                CSVReader csvReader = new CSVReader(new FileReader(file))) {
            String[] values = null;
            while (true) {
                try {
                    if (!((values = csvReader.readNext()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                records.add(Arrays.asList(values));
            }
        }
        return records;
    }


}
