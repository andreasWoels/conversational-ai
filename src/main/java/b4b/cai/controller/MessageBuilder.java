package b4b.cai.controller;

import b4b.cai.model.DataStore;
import b4b.cai.model.MessageFormat;
import b4b.cai.model.Task;
import b4b.cai.model.User;
import b4b.cai.model.conversational_ai.*;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class MessageBuilder {


    private static MessageBuilder instance;

    public static MessageBuilder getInstance(){

        if(instance == null){
            return new MessageBuilder();
        }
        return instance;
    }

    public List<Reply> getTextReplyList(User user, Integer counter, Conversation conversation) throws IOException, ParseException {

        Integer DATE_FIELD_OFFSET = 1;
        List<String> questions = HelperClass.getInstance().getQuestionsForToday();
        String question = questions.get(counter + DATE_FIELD_OFFSET);

        if(question.contains("<task>")){
            if(!user.getList().isEmpty()){

                question = question.replace("<task>", user.getHistory().get(user.getHistory().size() - 1).getName());
                conversation.getMemory().setQuestion(question.toString());
            }
            else{
                question = question.replace("<task>", "a specific task");
                conversation.getMemory().setQuestion(question.toString());
            }
        }

        List<Reply> listReplyList = new ArrayList<>();
        TextReply newListReply = new TextReply(question);
        listReplyList.add(newListReply);

        return listReplyList;
    }


    public List<Reply> wrapReplyContent(Content content) {

        Reply newListReply = new ListReply(content);
        List<Reply> listReplyList = new ArrayList<>();
        listReplyList.add(newListReply);

        return listReplyList;
    }

    public List<Reply> wrapReplyText(TextReply text) {

        List<Reply> listReplyList = new ArrayList<>();
        listReplyList.add(text);

        return listReplyList;
    }

    public MessageFormat buildAnswer(User user, Integer counter) throws IOException, ParseException {

        Conversation newConversation = new Conversation();

        if(counter + 1 == HelperClass.getInstance().getQuestionsForToday().size() )
        {
            newConversation.getMemory().setSk("4-1");
        }
        else{
            newConversation.getMemory().setSk("4");
            newConversation.getMemory().setQuestion(counter.toString());
        }


        newConversation.getMemory().setId(user.getId().toString());
        MessageFormat message = new MessageFormat(new ArrayList<>(), newConversation);

        return message;
    }


    public MessageFormat buildQuestion(User user, Integer question) throws IOException, ParseException {

        Conversation newConversation = new Conversation();
        List<Reply> listReplyList = this.getTextReplyList(user, question, newConversation);

        newConversation.getMemory().setSk("4");
        newConversation.getMemory().setId(user.getId().toString());
        newConversation.getMemory().setQuestion(question.toString());
        MessageFormat message = new MessageFormat(listReplyList, newConversation);

        return message;
    }


    public MessageFormat buildList(User user, List<ListItem> items, Memory memory){

        if(memory == null){

            memory = new Memory();
            memory.setId(user.getId().toString());
        }
        Conversation newConversation = new Conversation(memory);

        List<Reply> replies = null;
        if(items.isEmpty()){
            TextReply reply = new TextReply("There are no To-Do's in your list.");
            replies = this.wrapReplyText(reply);
        }
        else{
            List<Button> buttonList = new ArrayList<>();
            buttonList.add(new Button("Close", "web_url", ""));
            Content content = new Content(buttonList, items);
            replies = this.wrapReplyContent(content);
        }

        return new MessageFormat(replies, newConversation);
    }

    public MessageFormat buildSkillList(String id){

        Memory memory = new Memory();
        memory.setId(id);
        Conversation newConversation = new Conversation(memory);

        List<Reply> replies = new ArrayList<>();

        List<Button> bs1 = new ArrayList<>();

        ListItem item = new ListItem("Add a task to the list", "", "e.g. 'Please add a task to my list'", bs1);
        ListItem item2 = new ListItem("Delete a task from the list", "", "e.g. 'I want to delete a task'", bs1);
        ListItem item3 = new ListItem("Overview of all personal tasks", "", "e.g. 'Show me my tasklist", bs1);
        ListItem item4 = new ListItem("Start a Reflection Unit", "", "e.g. 'Start a reflection'", bs1);
        ListItem item5 = new ListItem("Assign a task to a peer", "", "e.g. 'Please assign a task to Georg'", bs1);
        ListItem item6 = new ListItem("Show other peers", "", "e.g. 'List all users of the chatbot'", bs1);
        ListItem item7 = new ListItem("Funny Story", "", "e.g. 'Can you amuse me?'", bs1);
        ListItem item8 = new ListItem("How are you bot?", "", "e.g. 'How are you?'", bs1);
        ListItem item9 = new ListItem("", "", "", bs1);

        List<ListItem> items = new ArrayList<>();
        items.add(item);
        items.add(item5);
        items.add(item2);
        items.add(item3);
        items.add(item7);
        items.add(item6);
        items.add(item4);
        items.add(item8);
        items.add(item9);

        TextReply advice = new TextReply("Above you can see the skill's description and how to activate it over the chat window. You can ask me anytime again.");

        List<Button> buttons = new ArrayList<>();
        Content content = new Content(buttons, items);
        ListReply skills = new ListReply(content);

        replies.add(skills);
        replies.add(advice);

        return new MessageFormat(replies, newConversation);
    }

    public List<ListItem> buildTaskList(User user){

        List<Task> list = user.getList();
        List<ListItem> returnList = new ArrayList<>();
        for (Integer i = 0; i < list.size(); i++){

            List<Button> buttons = new ArrayList<>();
            //buttons.add(new Button("Delete", "web_url","Delete" ));


            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.LLLL.yyyy HH:mm");
            String formattedString = list.get(i).getDate().format(formatter);

            ListItem newlistitem = new ListItem(list.get(i).getName(),
                    "", "Deadline: " + formattedString + "\n" + "Duration: " + list.get(i).getDuration() + "\n"
                    + "Priority: " + list.get(i).getPriority(), buttons);
            returnList.add(newlistitem);
        }

        return returnList;
    }

    public MessageFormat buildUserList(User user){

        List<User> list = DataStore.getInstance().getUsers();
        List<ListItem> returnList = new ArrayList<>();
        for (Integer i = 0; i < list.size(); i++){

            List<Button> buttons = new ArrayList<>();
            //buttons.add(new Button("Delete", "web_url","Delete" ));

//            LocalDateTime lastOnline = list.get(i).getAttendance().get(list.get(i).getAttendance().size() - 1);
            ListItem newlistitem = new ListItem(StringUtils.capitalize(list.get(i).getName()),"",
                    "" //+ lastOnline.getDayOfWeek() + " " + lastOnline.getHour() + ":" + lastOnline.getMinute()
            , buttons);
            returnList.add(newlistitem);
        }

        Memory memory = new Memory();
        memory.setId(user.getId().toString());
        Conversation newConversation = new Conversation(memory);

        List<Reply> replies = null;
        if(returnList.isEmpty()){
            TextReply reply = new TextReply("There are no other User's registered");
            replies = this.wrapReplyText(reply);
        }
        else{
            List<Button> buttonList = new ArrayList<>();
            buttonList.add(new Button("Close", "web_url", ""));
            Content content = new Content(buttonList, returnList);
            replies = this.wrapReplyContent(content);
        }

        return new MessageFormat(replies, newConversation);
    }

    public MessageFormat buildNewTask(User user) throws IOException {

        Memory memory = new Memory();
        memory.setId(user.getId().toString());

        Conversation newConversation = new Conversation(memory);

        List<Reply> replies = new ArrayList<>();
        replies.add(new TextReply("Your To-Do` List has a new entry!"));
        MessageFormat message = new MessageFormat(replies, newConversation);

        return message;
    }

    public MessageFormat buildText(User user, List<String> text, String skill) throws IOException {

        Memory memory = new Memory();
        memory.setId(user.getId().toString());
        if(skill != null){

            memory.setSk(skill);
        }

        Conversation newConversation = new Conversation(memory);
        List<Reply> replies = new ArrayList<>();

        for(Integer i = 0; i < text.size(); i++){
            replies.add(new TextReply(text.get(i)));
        }
        MessageFormat message = new MessageFormat(replies, newConversation);

        return message;
    }

    public MessageFormat buildTextWithoutUser(List<Reply> replies) throws IOException {

        Conversation newConversation = new Conversation();
        MessageFormat message = new MessageFormat(replies, newConversation);

        return message;
    }
    public MessageFormat buildTextWithMemory(List<Reply> replies, String skill, String id) throws IOException {

        Memory memory;
        if(skill == null){
            memory = new Memory();
            memory.setId(id);
        }
        else{
            memory = new Memory();
            memory.setSk(skill);
            memory.setId(id);
        }
        Conversation newConversation = new Conversation(memory);
        MessageFormat message = new MessageFormat(replies, newConversation);

        return message;
    }

    public MessageFormat buildTextWithCustomMemory(List<Reply> replies, Memory memory) throws IOException {


        Conversation newConversation = new Conversation(memory);
        MessageFormat message = new MessageFormat(replies, newConversation);

        return message;
    }

}
