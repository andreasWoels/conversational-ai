package b4b.cai.scheduler;

import b4b.cai.controller.HelperClass;
import b4b.cai.model.Conversation;
import b4b.cai.model.ConversationResponse;

import b4b.cai.model.DataStore;
import b4b.cai.model.User;
import b4b.cai.rest.CaiInterface;
import org.json.JSONException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

public class ReflectionJob implements Job
{

    private String conversationalUrl = "https://api.cai.tools.sap/connect/v1/conversations/";
    private String developerToken = "1712fd146137df17c2e93aba0097bacb";
    private String telegramChannelID = "ad5de6e5-afda-456a-bf8a-431c2128de00";
    private String teamsChannelID = "0c80e989-656d-4626-88b1-84e3d1a0d457";
    private String slackChannelID = "3a75fe38-ffcb-4767-8cae-aca39665ee49";

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {
            String response = CaiInterface.getInstance().executeGet(conversationalUrl, developerToken);
            ConversationResponse result = HelperClass.getInstance().getGson().fromJson(response, ConversationResponse.class);

            List<Conversation> conversations = result.getResults();
            List<Conversation> filteredConversations = conversations.stream().filter(x -> isRelevantChannel(x.getChannel())).collect(Collectors.toList());

            for(Integer i = 0; i < conversations.size(); i++){

                User correspondingUser = HelperClass.getInstance().findCorrespondingUser(conversations.get(i));

                if(correspondingUser != null){

                    LocalDateTime latest = null;
                    LocalTime timeFromDateTime = null;
                    if(!correspondingUser.getAttendance().isEmpty()){

                        latest = correspondingUser.getAttendance().get(correspondingUser.getAttendance().size() - 1);
                        timeFromDateTime = latest.toLocalTime();
                    }

                    if(latest == null){

                        sendMessage(conversations, i);
                    }
                    else if(HelperClass.getInstance().isSameDate(LocalDateTime.now(), latest)){

                        boolean isBefore = timeFromDateTime.isBefore(HelperClass.getInstance().getTimeseperator());
                        boolean isBeforeNow = LocalTime.now().isBefore(HelperClass.getInstance().getTimeseperator());
                        if(isBefore){
                            if(!isBeforeNow){

                                sendMessage(conversations, i);
                            }
                        }
                    }
                    else{
                        sendMessage(conversations, i);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendMessage(List<Conversation> conversations, Integer i) throws IOException, JSONException {
        String url = conversationalUrl + conversations.get(i).getId() +"/messages";

        String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \" "+ "This is a reminder for your half-day reflection. "  +"\" }]}";
        String res = CaiInterface.getInstance().executeHTTPRequest(url, developerToken, message, "POST");
    }

    public boolean isRelevantChannel(String channelID){

        if(channelID.equals(slackChannelID) || channelID.equals(telegramChannelID) || channelID.equals(teamsChannelID)){
            return true;
        }
        else{
            return false;
        }
    }
}

