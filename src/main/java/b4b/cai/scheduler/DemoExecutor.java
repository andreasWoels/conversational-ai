package b4b.cai.scheduler;

import java.util.concurrent.*;

public class DemoExecutor
{
    public static void main(String[] args)
    {
        Integer threadCounter = 0;
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(1);

        CustomThreadPoolExecutor executor = new CustomThreadPoolExecutor(1,
                1, 100, TimeUnit.DAYS, blockingQueue);

        // Let start all core threads initially
        executor.prestartAllCoreThreads();
        executor.execute(new CustomTask(threadCounter.toString()));
    }

}