package b4b.cai.scheduler;

import b4b.cai.controller.HelperClass;
import b4b.cai.model.*;
import b4b.cai.rest.CaiInterface;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private String conversationalUrl = "https://api.cai.tools.sap/connect/v1/conversations/";
    private String developerToken = "1712fd146137df17c2e93aba0097bacb";
    private String telegramChannelID = "ad5de6e5-afda-456a-bf8a-431c2128de00";
    private String teamsChannelID = "0c80e989-656d-4626-88b1-84e3d1a0d457";
    private String slackChannelID = "3a75fe38-ffcb-4767-8cae-aca39665ee49";


    @Scheduled(cron = "0 00 13 ? * MON-FRI")
    public void scheduleTaskWithCronExpression() {
        work();
    }

    @Scheduled(cron = "0 00 14 ? * MON-FRI")
    public void scheduleTaskWithCronExpression2() {
        work();
    }

    @Scheduled(cron = "0 00 05 ? * MON-FRI")
    public void scheduleTaskWithCronExpression3() {
        work();
    }

    @Scheduled(cron = "0 00 06 ? * MON-FRI")
    public void scheduleTaskWithCronExpression4() {
        work();
    }

    @Scheduled(cron = "0 01 00 ? * MON-FRI")
    public void scheduleTaskWithCronExpression5() throws IOException, JSONException {
        String CONVERSATION_URL = "https://api.cai.tools.sap/connect/v1/conversations/";
        String DEVELOPERTOKEN = "1712fd146137df17c2e93aba0097bacb";

        for (Integer i = 0; i < DataStore.getInstance().getUsers().size(); i++) {

            for (Integer j = 0; j < DataStore.getInstance().getUsers().get(i).getList().size(); j++) {

                Task task = DataStore.getInstance().getUsers().get(i).getList().get(j);
                if (HelperClass.getInstance().isOneDayBefore(task.getDate(), LocalDateTime.now())) {

                    String url = conversationalUrl + DataStore.getInstance().getUsers().get(i).getConversationId() +"/messages";
                    String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \"Reminder: Your task \"" + task.getName() + "\" should be done in the next 24 hours.\" }]}";
                    String res = CaiInterface.getInstance().executeHTTPRequest(url, DEVELOPERTOKEN, message, "POST");
                }

            }

        }
    }

    public void sendMessage(List<Conversation> conversations, Integer i) throws IOException, JSONException {
        String url = conversationalUrl + conversations.get(i).getId() +"/messages";

        String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \" "+ "This is a reminder for your half-day reflection. "  +"\" }]}";
        String res = CaiInterface.getInstance().executeHTTPRequest(url, developerToken, message, "POST");
    }

    public boolean isRelevantChannel(String channelID){

        if(channelID.equals(slackChannelID) || channelID.equals(telegramChannelID) || channelID.equals(teamsChannelID)){
            return true;
        }
        else{
            return false;
        }
    }


    public void work(){

        try {

            String response = CaiInterface.getInstance().executeGet(conversationalUrl, developerToken);
            ConversationResponse result = HelperClass.getInstance().getGson().fromJson(response, ConversationResponse.class);

            List<Conversation> conversations = result.getResults();
            List<Conversation> filteredConversations = conversations.stream().filter(x -> isRelevantChannel(x.getChannel())).collect(Collectors.toList());

            for(Integer i = 0; i < conversations.size(); i++){

                User correspondingUser = HelperClass.getInstance().findCorrespondingUser(conversations.get(i));

                if(correspondingUser != null){

                    LocalDateTime latest = null;
                    LocalTime timeFromDateTime = null;
                    if(!correspondingUser.getAttendance().isEmpty()){

                        latest = correspondingUser.getAttendance().get(correspondingUser.getAttendance().size() - 1);
                        timeFromDateTime = latest.toLocalTime();
                    }

                    if(latest == null){

                        sendMessage(conversations, i);
                    }
                    else if(HelperClass.getInstance().isSameDate(LocalDateTime.now(), latest)){

                        boolean isBefore = timeFromDateTime.isBefore(HelperClass.getInstance().getTimeseperator());
                        boolean isBeforeNow = LocalTime.now().isBefore(HelperClass.getInstance().getTimeseperator());
                        if(isBefore){
                            if(!isBeforeNow){

                                sendMessage(conversations, i);
                            }
                        }
                    }
                    else{
                        sendMessage(conversations, i);
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println("error running thread " + ex.getMessage());
        }
    }
}