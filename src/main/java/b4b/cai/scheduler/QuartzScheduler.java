package b4b.cai.scheduler;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.HashMap;

public class QuartzScheduler
{

    private HashMap<String, Trigger> triggerHashMap;

    public static void main() throws Exception
    {

        JobDetail earlyJob = JobBuilder.newJob(ReflectionJob.class)
                .withIdentity("earlyJob", "group1").build();
        JobDetail lateJob = JobBuilder.newJob(ReflectionJob.class)
                .withIdentity("lateJob", "group1").build();
        JobDetail earlyRemindJob = JobBuilder.newJob(ReflectionJob.class)
                .withIdentity("earlyRemindJob", "group2").build();
        JobDetail lateRemindJob = JobBuilder.newJob(ReflectionJob.class)
                .withIdentity("lateJob", "group2").build();

        JobDetail remindJob = JobBuilder.newJob(QuartzReminder.class)
                .withIdentity("remindJob", "group1").build();


        String earlyRepetition = "0 07 10 ? * MON-FRI";
        String earlyReminder = "0 08 10 ? * MON-FRI";
        String lateRepetition = "0 00 15 ? * MON-FRI";
        String lateReminder = "0 00 16 ? * MON-FRI";
        String repetition = "0 35 9 ? * MON-FRI";
        String testrepetition = "0/10 * * * * ?";

        Trigger earlyTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("earlyTrigger", "group1")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(testrepetition))
                .build();

        Trigger earlyRemindTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("earlyRemindTrigger", "group1")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(earlyReminder))
                .build();

        Trigger lateTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("lateTrigger", "group2")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(lateRepetition))
                .build();

        Trigger lateRemindTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("lateRemindTrigger", "group2")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(lateReminder))
                .build();

        Trigger remindTrigger = TriggerBuilder
                .newTrigger()
                .withIdentity("remindTrigger", "group1")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(repetition))
                .build();

        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
        scheduler.scheduleJob(earlyJob, earlyTrigger);
        scheduler.scheduleJob(lateJob, lateTrigger);
        scheduler.scheduleJob(earlyRemindJob, earlyRemindTrigger);
        scheduler.scheduleJob(lateRemindJob, lateRemindTrigger);
        scheduler.scheduleJob(remindJob, remindTrigger);

    }
}
