package b4b.cai.scheduler;

import b4b.cai.controller.HelperClass;
import b4b.cai.controller.MessageHandler;
import b4b.cai.model.DataStore;
import b4b.cai.model.Task;
import b4b.cai.rest.CaiInterface;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.time.LocalDateTime;



public class QuartzReminder implements Job {

    String CONVERSATION_URL = "https://api.cai.tools.sap/connect/v1/conversations/";
    String DEVELOPERTOKEN = "1712fd146137df17c2e93aba0097bacb";

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        try {

            for (Integer i = 0; i < DataStore.getInstance().getUsers().size(); i++) {

                for (Integer j = 0; j < DataStore.getInstance().getUsers().get(i).getList().size(); j++) {

                    Task task = DataStore.getInstance().getUsers().get(i).getList().get(j);
                    if (HelperClass.getInstance().isOneDayBefore(task.getDate(), LocalDateTime.now())) {

                        String url = CONVERSATION_URL + DataStore.getInstance().getUsers().get(i).getConversationId() +"/messages";
                        String message = "{ \"messages\": [{ \"type\": \"text\", \"content\": \"The task \"" + task.getName() + "\" that should be done in the next 24 hours.\" }]}";
                        String res = CaiInterface.getInstance().executeHTTPRequest(url, DEVELOPERTOKEN, message, "POST");
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
